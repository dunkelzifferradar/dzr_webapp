const replace = require('replace-in-file');
const package = require('../package.json');
const buildVersion = process.env.CI_COMMIT_TAG.replace(/\./gi, "-");
const options = {
  files: './apps/frontend/src/environments/environment.prod.ts',
  from: /apiUrl: 'https:\/\/app\.covid19\.dunkelzifferradar\.de'/g,
  to: 'apiUrl: \'https://app.covid19.dunkelzifferradar.de/' + buildVersion + '\'',
  allowEmptyPaths: false
};

try {
  let changedFiles = replace.sync(options);
  if (changedFiles == 0) {
    throw 'Please make sure that file \'' + options.files + '\' has "version: \'\'"';
  }
  console.log('Build version set: ' + buildVersion);
} catch (error) {
  console.error('Error occurred:', error);
  throw error;
}
