#!/usr/bin/env bash

if [ -d "${1}" ]; then
  ENV_PATH="${1}"
else
  ENV_PATH="./.env.sample"
fi

echo "${ENV_PATH}"

URL=$(grep URL "${ENV_PATH}" | cut -d '=' -f2)
POSTGRES_USERNAME=$(grep POSTGRES_USERNAME "${ENV_PATH}" | cut -d '=' -f2)
POSTGRES_PASSWORD=$(grep POSTGRES_PASSWORD "${ENV_PATH}" | cut -d '=' -f2)

( cd ./tools/liquibase; \
  liquibase \
  --url="${URL}" \
  --username="${POSTGRES_USERNAME}" \
  --password="${POSTGRES_PASSWORD}" \
  update \
) || echo MEANINGLESS ERROR: something went wrong
