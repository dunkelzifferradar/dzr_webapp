[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

# WeVsVirus

## Initial Setup

This project runs on `node`. For initial setup run `npm ci`.

## Run the App Locally

Run `npm run start` to run both the frontend and backend.

To only run the frontend or the backend, call `npm run start:frontend`
or `npm run start:backend`, respectively.

### Local Database

Run `npm run start-db` to _start_ the local database.
Run `npm run stop-db` to _stop_ the local database.

#### Schema

> TODO: The schema is supposed to live in another repo, which makes
> local setup difficult.

## Building the App

Run `npm run build` to build both frontend and backend.
The final build will end up in `dist/apps/{backend|frontend}`

To only build the frontend or the backend, call `npm run build:frontend` or
`npm run build:backend`, respectively.

The backend build consists only of a single `main.js` file and its
source map `main.js.map`, and should be run via `node main.js`.

The frontend is a normal "static" folder which should be served via
any old webserver.

The built frontend and backend do not work together though,
as the proxy from the frontend to the backend is not part of the build,
and therefore the frontend does not know how to reach the backend.
This connection is part of the deployment process instead.

### Docker and Deployment

In order to run the built apps, we need to package them into docker containers,
in which they will run when these containers come up.

> TODO: This involves proxying the frontend `api/` requests to the backend
> running in a different container.
> The backend URI however is not known at build time, so we need to
> inject that info into the frontend container at run time (probably via
> environment variables).

## Contributing and Local Development

### Commit Messages

Commit messages should follow the [conventional commit structure](https://www.conventionalcommits.org/en/v1.0.0/).

If you are unfamiliar with conventional commit, you can run `npm run commit`
instead of `git commit`, which will run a tool on the command line which
will help author the message.

## Generated README

> Below follows the generated README from `npx create-nx-workspace@latest`,
> which was used to set up this project structure.

This project was generated using [Nx](https://nx.dev).

<p align="center"><img src="https://raw.githubusercontent.com/nrwl/nx/master/nx-logo.png" width="450"></p>

🔎 **Nx is a set of Extensible Dev Tools for Monorepos.**

## Quick Start & Documentation

[Nx Documentation](https://nx.dev/angular)

[10-minute video showing all Nx features](https://nx.dev/angular/getting-started/what-is-nx)

[Interactive Tutorial](https://nx.dev/angular/tutorial/01-create-application)

## Adding capabilities to your workspace

Nx supports many plugins which add capabilities for developing different types of applications and different tools.

These capabilities include generating applications, libraries, etc as well as the devtools to test, and build projects as well.

Below are some plugins which you can add to your workspace:

- [Angular](https://angular.io)
  - `ng add @nrwl/angular`
- [React](https://reactjs.org)
  - `ng add @nrwl/react`
- Web (no framework frontends)
  - `ng add @nrwl/web`
- [Nest](https://nestjs.com)
  - `ng add @nrwl/nest`
- [Express](https://expressjs.com)
  - `ng add @nrwl/express`
- [Node](https://nodejs.org)
  - `ng add @nrwl/node`

## Generate an application

Run `ng g @nrwl/angular:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `ng g @nrwl/angular:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are sharable across libraries and applications. They can be imported from `@we-vs-virus/mylib`.

## Development server

Run `ng serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng g component my-component --project=my-app` to generate a new component.

## Build

Run `ng build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `ng e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx dep-graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev/angular) to learn more.
