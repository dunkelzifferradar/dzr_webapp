import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { chunk, meanBy } from 'lodash';

import { CasesService } from '../cases/cases.service';
import {
  CHART_COLOR_CASES,
  CHART_COLOR_UNREPORTED,
  ICaseResponse
} from '@we-vs-virus/interfaces';

import { MapState, MapStateModel } from '../redux/map.actions';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { HostListener } from '@angular/core';

@Component({
  selector: 'we-vs-virus-chart-bar-active-cases',
  templateUrl: './chart-bar-active-cases.component.html',
  styleUrls: ['./chart-bar-active-cases.component.less']
})
export class ChartBarActiveCasesComponent implements OnInit {
  constructor(private casesService: CasesService) {
    this.onResize();
  }

  @Select(MapState)
  public mapState$: Observable<MapStateModel>;

  single: any[];
  multi: any[];

  title: string = 'Aktive Infektionen';

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle: string = 'Legende';
  showXAxisLabel = false;
  showYAxisLabel = false;

  // TODO: we might want to change this dynamically
  //       depending on the viewport size
  groupPadding = 4;
  barPadding = 4;

  colorScheme = {
    domain: [CHART_COLOR_CASES, CHART_COLOR_UNREPORTED]
  };
  screenHeight: number;
  screenWidth: number;

  private readonly DAYS_AGGR_BAR_CHART = 7;

  public ngOnInit(): void {
    this.mapState$.subscribe(state => this.refreshChart(state));
  }

  public refreshChart(state: MapStateModel): void {
    this.casesService
      .getData(
        state.rkiIds,
        state.currentLevel,
        state.showTotalTimeframe ? moment(moment.utc().startOf('day')).subtract(364, 'days'): state.minDate,
        state.maxDate,
        undefined,
        state.expectedIfr
      )
      .subscribe((response: ICaseResponse[]) => {
        // parse backend response
        const multi = response.map(o => this.parseBackendResponse(o));

        if (this.isSmallScreen()) {
          this.multi = this.aggregateForSmallScreens(multi);
        } else {
          this.multi = multi;
        }
      });
  }

  private aggregateForSmallScreens(
    multi: { name: string; series: { name: string; value: number }[] }[]
  ) {
    const chunks = chunk(multi, this.DAYS_AGGR_BAR_CHART);
    return chunks.map(subArray =>
      this.formatDataForBarChart(
        subArray[0].name,
        Math.round(meanBy(subArray, entry => entry.series[0].value)),
        Math.round(meanBy(subArray, entry => entry.series[1].value))
      )
    );
  }

  private parseBackendResponse(
    o: ICaseResponse
  ): { name: string; series: { name: string; value: number }[] } {
    return this.formatDataForBarChart(
      new Date(o.date).toLocaleDateString(undefined, {
        month: 'numeric',
        day: 'numeric'
      }),
      o.casesActive,
      o.infectionsActive - o.casesActive
    );
  }

  private formatDataForBarChart(
    dateString: string,
    casesActive: number,
    unreportedInfectionsActive: number
  ) {
    return {
      name: dateString,
      series: [
        {
          name: 'gemeldet',
          value: casesActive
        },
        {
          name: 'ungemeldet',
          value: unreportedInfectionsActive
        }
      ]
    };
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }

  private isSmallScreen() {
    return this.screenWidth < 960;
  }

  legendPosition() {
    if (this.isSmallScreen()) {
      return 'below';
    } else return 'right';
  }
}
