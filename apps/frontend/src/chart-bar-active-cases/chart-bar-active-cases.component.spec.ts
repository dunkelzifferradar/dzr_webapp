import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartBarActiveCasesComponent } from './chart-bar-active-cases.component';

describe('ChartBarNewCasesComponent', () => {
  let component: ChartBarActiveCasesComponent;
  let fixture: ComponentFixture<ChartBarActiveCasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChartBarActiveCasesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartBarActiveCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
