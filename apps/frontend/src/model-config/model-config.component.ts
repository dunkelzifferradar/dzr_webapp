import { Component, OnInit, Inject } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { IfrSelectionChanged } from '../redux/map.actions';
import { Store } from '@ngxs/store';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExpectedIfr } from '@we-vs-virus/interfaces';

// ifr selection choices in order that UI shows them in.
const EXPECTED_IFR_FIELDS = [ExpectedIfr.LANCET, ExpectedIfr.HEINSBERG, ExpectedIfr.IOANNIDIS];

@Component({
  selector: 'we-vs-virus-model-config',
  templateUrl: './model-config.component.html',
  styleUrls: ['./model-config.component.scss']
})
export class ModelConfigComponent implements OnInit {

  constructor(private store: Store,
    public dialogRef: MatDialogRef<ModelConfigComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {selectedIfr: ExpectedIfr}) {}

  title: string = 'Konfiguriere das Dunkelziffer-Modell';
  ngOnInit(): void {}

  /**
   * Toggle handler, bound to the template
   */
  onChangeIfr = (event: MatRadioChange) => {
    this.store.dispatch(new IfrSelectionChanged(this.getExpectedIfr(event.value)));
  };

  getExpectedIfr(selectedOption: number) {
    return EXPECTED_IFR_FIELDS[selectedOption-1];
  }
}
