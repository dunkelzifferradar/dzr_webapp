import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { empty, Observable, of } from 'rxjs';

import { GeoJSONHierarchyLevel, ICaseResponse, ExpectedIfr } from '@we-vs-virus/interfaces';
import * as moment from 'moment';
import { Moment } from 'moment';
import { map } from 'rxjs/operators';

const PARAM_INFECT_ASYMP: number = 7;
const PARAM_INFECT_SYMP: number = 14;

@Injectable({
  providedIn: 'root'
})
export class CasesService {
  casesData: Map<string, ICaseResponse[]> = new Map();

  constructor(private httpClient: HttpClient) {}

  /**
   * Fetch model data for given time frame, level and feature IDs.
   *
   * @param rkiIds At minimum the country; at most country, state, county.
   * @param level {@link GeoJSONHierarchyLevel} which endpoint to ask
   * @param minDate first day (inclusive) of time frame.
   * @param maxDate last day (inclusive) of time frame.
   * @param modelId 0 for dummy model; 1 for first "real" model. Default=1.
   * @param expectedIfr the expected ifr to be used in the modelling.
   */
  public getData(
    rkiIds: Array<number>,
    level: GeoJSONHierarchyLevel,
    minDate: Moment,
    maxDate: Moment,
    modelId?: number,
    expectedIfr?: number,
    paramInfectSymp?: number,
    paramInfectAsymp?: number,
  ): Observable<ICaseResponse[]> {
    if (level === undefined) return empty();
    modelId = modelId === undefined ? 1 : modelId;

    expectedIfr = expectedIfr === undefined ? ExpectedIfr.LANCET: expectedIfr;
    paramInfectSymp = paramInfectSymp === undefined ? PARAM_INFECT_SYMP: paramInfectSymp;
    paramInfectAsymp = paramInfectAsymp === undefined ? PARAM_INFECT_ASYMP: paramInfectAsymp;

    // check if data for that level-id-model-dates-ifr combination has already been cached
    const cacheId = `${level}-${
      rkiIds[rkiIds.length - 1]
    }-${modelId}-${minDate.toISOString()}-${maxDate.toISOString()}-${expectedIfr.toString()}`;
    return this.casesData.has(cacheId)
      ? of(this.casesData.get(cacheId))
      : this.fetchCasesData(rkiIds, level, minDate, maxDate, modelId, expectedIfr, paramInfectSymp, paramInfectAsymp, cacheId);
  }

  /**
   * Fetch and cache data for not-previously-made requests.
   */
  private fetchCasesData(
    rkiIds: Array<number>,
    level: GeoJSONHierarchyLevel,
    minDate: Moment,
    maxDate: Moment,
    modelId: number,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number,
    cacheId: string
  ) {
    return this.fetchRawData(rkiIds, level, minDate, maxDate, expectedIfr, paramInfectSymp, paramInfectAsymp, modelId).pipe(
      map(response => {
        response.sort(this.sortByDates);

        // cache the response
        this.casesData.set(cacheId, response);
        return response;
      })
    );
  }

  private fetchRawData(
    rkiIds: Array<number>,
    level: GeoJSONHierarchyLevel,
    minDate: Moment,
    maxDate: Moment,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number,
    modelId: number
  ): Observable<ICaseResponse[]> {
    const urlTail = rkiIds.join('/');
    const params = this.createParams(minDate, maxDate, expectedIfr, paramInfectSymp, paramInfectAsymp);
    return this.httpClient.get<ICaseResponse[]>(
      `api/cases-${level.toLowerCase()}/${modelId}/${urlTail}`,
      { params: params }
    );
  }

  private createParams(minDate: Moment, maxDate: Moment, expectedIfr: number, paramInfectSymp: number, paramInfectAsymp: number) {
    return {
      minDate: minDate.toISOString(),
      maxDate: maxDate.toISOString(),
      expectedIfr: expectedIfr.toString(),
      paramInfectSymp: paramInfectSymp.toString(),
      paramInfectAsymp: paramInfectAsymp.toString()
    };
  }
  private sortByDates(a: ICaseResponse, b: ICaseResponse) {
    const dateA = moment(a.date);
    const dateB = moment(b.date);
    return dateA > dateB ? 1 : -1;
  }
}
