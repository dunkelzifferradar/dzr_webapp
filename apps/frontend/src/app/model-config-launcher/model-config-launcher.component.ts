import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModelConfigComponent } from '../../model-config/model-config.component';
import { ExpectedIfr } from '@we-vs-virus/interfaces';

@Component({
  selector: 'we-vs-virus-model-config-launcher',
  templateUrl: './model-config-launcher.component.html',
  styleUrls: ['./model-config-launcher.component.css']
})
export class ModelConfigLauncherComponent implements OnInit {
  selectedIfrOption: ExpectedIfr = ExpectedIfr.LANCET;

  constructor(private modelInfoDialog: MatDialog) { }

  ngOnInit(): void {
  }

  /**
   * Open the model information in a popup dialog.
   */
  openDialog() {
    const dialogRef = this.modelInfoDialog.open(ModelConfigComponent, {
      data: {selectedIfr: this.selectedIfrOption},
      panelClass: 'custom-dialog-container'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.selectedIfrOption = result;
    });
  }

}
