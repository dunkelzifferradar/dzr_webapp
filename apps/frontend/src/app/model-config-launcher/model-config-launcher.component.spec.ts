import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelConfigLauncherComponent } from './model-config-launcher.component';

describe('ModelConfigLauncherComponent', () => {
  let component: ModelConfigLauncherComponent;
  let fixture: ComponentFixture<ModelConfigLauncherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelConfigLauncherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelConfigLauncherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
