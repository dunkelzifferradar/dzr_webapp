import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDemoDataResponse } from '@we-vs-virus/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DemodataService {
  constructor(private httpClient: HttpClient) {}

  getData(): Observable<IDemoDataResponse> {
    return this.httpClient.get<IDemoDataResponse>(`api/demodata`);
  }
}
