import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { empty, Observable, of } from 'rxjs';
import { FeatureCollection, GeoJsonObject } from 'geojson';
import { GeoJSONHierarchyLevel } from '@we-vs-virus/interfaces';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeoJSONService {
  private geoJsonData: Map<string, FeatureCollection> = new Map();

  constructor(private httpClient: HttpClient) {}

  /**
   * Get the Geo JSON layer for a specific level.
   */
  public getData: (
    level: GeoJSONHierarchyLevel,
    rkiIds: Array<number>
  ) => Observable<FeatureCollection> = (level, rkiIds) => {
    if (level === undefined) return empty();

    // check if Geo JSON data for that level-id combination has already been cached
    const cacheId = `${level}-${rkiIds[rkiIds.length - 1]}`;
    return this.geoJsonData.has(cacheId)
      ? // cached -> directly return it as an observable
        of(this.geoJsonData.get(cacheId))
      : // not cached -> fetch & return it as an observable
        this.fetchGeoJSONData(level, rkiIds);
  };

  /**
   * Fetch Geo JSON data from the backend, fix the feature geometry, and cache
   * the new data.
   */
  private fetchGeoJSONData: (
    level: GeoJSONHierarchyLevel,
    rkiIds: Array<number>
  ) => Observable<FeatureCollection> = (level, rkiIds) => {
    return this.fetchRawData(level, rkiIds).pipe(
      map((response: any) => {
        // fix feature geometry
        const featureCollection: FeatureCollection = {
          ...response,
          features: response.features.map(feature => ({
            ...feature,
            geometry: JSON.parse(feature.geometry.split("'").join('"'))
          }))
        };

        // cache the new Geo JSON data
        this.geoJsonData.set(
          `${level}-${rkiIds[rkiIds.length - 1]}`,
          featureCollection
        );

        // return it
        return featureCollection;
      })
    );
  };

  private fetchRawData(
    level: GeoJSONHierarchyLevel,
    rkiIds: Array<number>
  ): Observable<GeoJsonObject> {
    const urlTail = rkiIds.join('/');
    return this.httpClient.get<GeoJsonObject>(
      `api/geojson-${level.toLowerCase()}/${urlTail}`
    );
  }
}
