import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';


import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { environment } from '../environments/environment';
import { APIInterceptor } from './api.interceptor';
import { AppComponent } from './app.component';
import { ChartLineAccumulatedCasesComponent } from '../chart-line-accumulated-cases/chart-line-accumulated-cases.component';
import { ChartBarActiveCasesComponent } from '../chart-bar-active-cases/chart-bar-active-cases.component';
import { MapComponent } from '../map/map.component';
import { MapLegendComponent } from '../map/map-legend/map-legend.component';
import { ModelConfigComponent } from '../model-config/model-config.component';

import { MapState } from '../redux/map.actions';
import { ModelConfigLauncherComponent } from './model-config-launcher/model-config-launcher.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartLineAccumulatedCasesComponent,
    ChartBarActiveCasesComponent,
    MapComponent,
    MapLegendComponent,
    ModelConfigComponent,
    ModelConfigLauncherComponent
  ],
  entryComponents: [
    ModelConfigComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    LeafletModule,
    NgxsModule.forRoot([MapState]),
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSlideToggleModule,
    MatListModule,
    MatRadioModule,
    MatDialogModule,
    MatTooltipModule
  ],
  providers: [
    { provide: 'BASE_API_URL', useValue: environment.apiUrl },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
