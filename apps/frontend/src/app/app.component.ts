import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { MapState } from '../redux/map.actions';
import { GeoJSONService } from './geojson.service';

@Component({
  selector: '#dzr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private store: Store,
    private geoJsonService: GeoJSONService) {}

  @Select(MapState.rkiIds)
  rkiIds$: Observable<Array<number>>;

  DEFAULT_TITLE: String = 'Deutschland';
  title: String = this.DEFAULT_TITLE;

  ngOnInit(): void {
    this.rkiIds$.subscribe(this.onRkiIdsUpdate);
  }

  onRkiIdsUpdate = (rkiIds: Array<number>) => {
    if (rkiIds.length <= 1) {
      // topmost level => set title to default
      this.title = this.DEFAULT_TITLE;
    } else {
      const currentLevel = this.store.selectSnapshot(MapState.currentLevel);

      // any other level => set title to level name
      this.geoJsonService
        .getData(currentLevel, rkiIds)
        .subscribe(({ features }) => {
          // find name and set it as title
          this.title =
            features[0].properties.name || features[0].properties.gen;
        });
    }
  };
}
