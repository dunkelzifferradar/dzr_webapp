import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartLineAccumulatedCasesComponent } from './chart-line-accumulated-cases.component';

describe('ChartLinePredictedCasesComponent', () => {
  let component: ChartLineAccumulatedCasesComponent;
  let fixture: ComponentFixture<ChartLineAccumulatedCasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartLineAccumulatedCasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartLineAccumulatedCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
