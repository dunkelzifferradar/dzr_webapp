import { Component, OnInit } from '@angular/core';
import {
  CHART_COLOR_CASES,
  CHART_COLOR_INFECTIONS,
  GeoJSONHierarchyLevel,
  ICaseResponse
} from '@we-vs-virus/interfaces';
import { CasesService } from '../cases/cases.service';
import { Select, Store } from '@ngxs/store';
import { MapState, MapStateModel, TotalTimeframeToggled } from '../redux/map.actions';
import { Observable } from 'rxjs';
import { Moment } from 'moment';
import * as moment from 'moment';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { HostListener } from "@angular/core";

@Component({
  selector: 'we-vs-virus-chart-line-predicted-cases',
  templateUrl: './chart-line-accumulated-cases.component.html',
  styleUrls: ['./chart-line-accumulated-cases.component.less']
})
export class ChartLineAccumulatedCasesComponent implements OnInit {
  constructor(private casesService: CasesService,
    private store: Store) {
      this.onResize();
    }

  public chartdata: any[];

  @Select(MapState)
  public mapState$: Observable<MapStateModel>;

  title: string = 'Alle Infektionen';

  // options
  legend: boolean = true;
  yScaleMin: number = 0;
  yScaleMax: number;
  legendTitle: string = 'Legende';
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  yAxisTicks: number[] = [];
  showYAxisLabel: boolean = false;
  showXAxisLabel: boolean = false;
  timeline: boolean = true;
  colorScheme = {
    domain: [CHART_COLOR_INFECTIONS, CHART_COLOR_CASES]
  };
  screenHeight: number;
  screenWidth: number;

  public ngOnInit(): void {
    this.mapState$.subscribe(state =>
      this.refreshChart(
        state.rkiIds,
        state.currentLevel,
        state.showTotalTimeframe ? moment(moment.utc().startOf('day')).subtract(364, 'days') : state.minDate,
        state.maxDate,
        state.expectedIfr
      )
    );
  }

  private refreshChart(
    featureIds: Array<number>,
    level: GeoJSONHierarchyLevel,
    minDate: Moment,
    maxDate: Moment,
    expectedIfr: number
  ): void {
    this.casesService
      .getData(featureIds, level, minDate, maxDate, undefined, expectedIfr)
      .subscribe((response: ICaseResponse[]) => {
        this.chartdata = [
          {
            name: 'gesamt',
            series: response.map(responseDay => {
              return {
                value: responseDay.infectionsSum,
                name: new Date(responseDay.date).toLocaleDateString(undefined, {
                  month: 'numeric',
                  day: 'numeric'
                })
              };
            })
          },
          {
            name: 'gemeldet',
            series: response.map(responseDay => {
              return {
                value: responseDay.casesSum,
                name: new Date(responseDay.date).toLocaleDateString(undefined, {
                  month: 'numeric',
                  day: 'numeric'
                })
              };
            })
          }
        ];

        this.setYScale(response);
      });
  }

  private setYScale(response: ICaseResponse[]) {
    // Find max infection value
    const maxInfections = Math.max(
      0,
      ...response.map(({ infectionsSum }) => infectionsSum)
    );
    const minCases = Math.min(0, ...response.map(({ casesSum }) => casesSum));
    this.yScaleMax = maxInfections * 1.2;
    this.yScaleMin = minCases * 0.8;
    this.yAxisTicks = [
      this.smartRounding(minCases),
      this.smartRounding(maxInfections * 0.5),
      this.smartRounding(maxInfections)
    ];
  }

  private smartRounding(number: number): number {
    const len = (number + '').length;
    const fac = Math.pow(10, len - 1);
    return Math.round(number / fac) * fac;
  }

  /**
   * Toggle handler, bound to the template
   */
  onToggleTotalTimeframe = (event: MatSlideToggleChange) => {
    this.store.dispatch(new TotalTimeframeToggled(event.checked));
  };

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
   this.screenHeight = window.innerHeight;
   this.screenWidth = window.innerWidth;
  }

  legendPosition() {
    if (this.screenWidth < 960) {
      return "below";
    }
    else return "right";
  }
}
