import { Feature, FeatureCollection } from 'geojson';
import { GeoJSONHierarchyLevel } from '@we-vs-virus/interfaces';

/**
 * Helper method to find a feature by its ID ("rkiLevelId") in the Geo JSON data
 */
export const findFeatureByID: (
  featureCollection: FeatureCollection,
  level: GeoJSONHierarchyLevel,
  featureId: number
) => Feature = (featureCollection, level, featureId) => {
  return featureCollection.features.find(
    ({ properties }) => properties[`rki${level}Id`] === featureId
  );
};

export function getColor(d) {
  return d > 300
    ? '#e65100'
    : d > 150
    ? '#ef6c00'
    : d > 50
    ? '#f57b00'
    : d > 35
    ? '#ff9801'
    : d > 10
    ? '#ffb74d'
    : d !== undefined
    ? '#ffe0b1'
    : 'rgba(255,255,255,.1)';
}
