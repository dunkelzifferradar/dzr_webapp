import { Component, OnInit } from '@angular/core';
import { getColor } from '../../map/map-helpers';

@Component({
  selector: 'we-vs-virus-map-legend',
  templateUrl: './map-legend.component.html',
  styleUrls: ['./map-legend.component.css']
})
export class MapLegendComponent implements OnInit {
  constructor() {}

  legendTitle = 'Aktuelle Infektionsdichte (pro 100.000)';

  textOver300 = '> 300';
  text150300 = '150-300';
  text50150 = '50-150';
  text3550 = '35-50';
  text1035 = '10-35';
  textUnder10 = '< 10';

  ngOnInit(): void {}

  getStyle(number: number) {
    return getColor(number);
  }
}
