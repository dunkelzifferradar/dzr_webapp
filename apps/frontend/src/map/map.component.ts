import { Component, OnInit } from '@angular/core';
import {
  Browser,
  GeoJSON,
  geoJSON,
  latLng,
  LatLngBounds,
  Layer,
  Map as LeafletMap,
  tileLayer
} from 'leaflet';
import { Feature, FeatureCollection } from 'geojson';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { GeoJSONService } from '../app/geojson.service';
import {
  BackButtonClicked,
  MapState,
  SelectFeature,
  MapStateModel
} from '../redux/map.actions';
import { getColor } from './map-helpers';
import { CasesService } from '../cases/cases.service';
import {
  GeoJSONHierarchy,
  GeoJSONHierarchyLevel,
  ICaseResponse,
  PER_POPULATION_MULTIPLIER
} from '@we-vs-virus/interfaces';
import * as moment from 'moment';
import { CasesMarker } from '../cases-marker/cases-marker';

@Component({
  selector: 'we-vs-virus-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  constructor(
    private geoJsonService: GeoJSONService,
    private casesService: CasesService,
    private store: Store
  ) {}


  @Select(MapState)
  public mapState$: Observable<MapStateModel>;

  map: LeafletMap;
  fitBounds: LatLngBounds;
  layers: Array<GeoJSON> = [
    //this.tileLayer //
  ];
  options = {
    zoom: 5,
    zoomAnimation: false,
    center: latLng(49.4148182, 8.7011091, 14),
    zoomControl: false,
    scrollWheelZoom: false,
    zoomSnap: 0.25
  };

  backBtnTitle: String = '';

  /**
   * OSM background map, currently not used.
   */
  tileLayer = tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '...'
  });

  /**
   * Currently not in use
   */
  ngOnInit() {}

  /**
   * Back button click handler, bound to the template
   */
  onClickBackButton = () => {
    this.store.dispatch(new BackButtonClicked());
  };

  /**
   * Generate a style object for a map Feature.
   */
  style(feature) {
    return {
      fillColor: getColor(feature.properties.casesPer100000),
      fillOpacity: 1,
      weight: 1,
      color: '#fff'
    };
  }

  /**
   * Event handlers for a map feature.
   * @param omitFeatureInteractions
   */
  onEachFeature(omitFeatureInteractions: boolean) {
    return (feature: Feature, featureLayer: Layer) => {
      if (!omitFeatureInteractions) {
        featureLayer.on({
          mouseover: this.onMouseOver,
          mouseout: this.resetHighlight,
          click: this.onSelectFeature
        });
      }
    };
  }

  onMouseOver = e => {
    this.highlightFeature(e);
    e.target
      .bindTooltip(e.target.feature.properties.gen, {
        direction: 'center',
        className: 'feature-tooltip',
        opacity: 0.85
      })
      .openTooltip();
  };

  /**
   * Event handler for the mouseover event on a feature.
   */
  highlightFeature = e => {
    const layer = e.target;

    layer.setStyle({
      fillOpacity: 0.9
    });

    if (!Browser.ie && !Browser.opera && !Browser.edge) {
      layer.bringToFront();
    }
  };

  /**
   * Event handler for the mouseout event on a feature.
   */
  resetHighlight = e => {
    this.layers[0].resetStyle(e.target);
  };

  /**
   * Feature click handler
   */
  onSelectFeature = e => {
    this.store.dispatch(new SelectFeature(e.target));
  };

  /**
   * Event handler for the leafletMapReady event.
   */
  onMapReady = (leafletMap: LeafletMap) => {
    // let's keep a reference to the map itself
    this.map = leafletMap;

    // set up the levels update subscription
    // this ultimately also provides initial map layers & bounds
    this.mapState$.subscribe(state => this.onMapStateUpdate(state.currentLevel, state.rkiIds));

    // FIXME: double click interferes with this.flyToFeature because it
    //        handles the first click of the double click, which it shouldn't
    this.map.doubleClickZoom.disable();

    // resize map on viewport resize
    this.map.on('resize', () => {
      if (this.fitBounds) {
        this.map.fitBounds(this.fitBounds);
      }
    });
  };

  /**
   * Update layers and bounds on hierarchy level and rkiIds update. Render map.
   */
  onMapStateUpdate = (currentLevel: GeoJSONHierarchyLevel, rkiIds: Array<number>) => {
    this.fitMapBounds(currentLevel, rkiIds);
    this.createDataMap(currentLevel, rkiIds);
  };

  /**
   * Fit map bounds to current level & selected area
   *
   * @param currentLevel
   * @param rkiIds
   */
  private fitMapBounds(
    currentLevel: GeoJSONHierarchyLevel,
    rkiIds: Array<number>
  ) {
    // get geojson for current level to get its bounds
    this.geoJsonService
      .getData(currentLevel, rkiIds)
      .subscribe(featureCollection => {
        // find bounds
        const feature = geoJSON(featureCollection.features[0]);
        // set bounds
        this.fitBounds = feature.getBounds();
      });

    // get geojson for previous level to get its name (for back button)
    this.geoJsonService
      .getData(GeoJSONHierarchy.get(currentLevel).previous, rkiIds.slice(0, -1))
      .subscribe(({ features }) => {
        // find name and set it as back button title
        this.backBtnTitle =
          features[0].properties.name || features[0].properties.gen;
      });
  }

  /**
   * Fetch geoJson and cases data for either current (if COUNTY) or next (if COUNTRY or STATE) level.
   * Display on the map via both colors and markers.
   *
   * @param currentLevel
   * @param rkiIds
   */
  private createDataMap(
    currentLevel: GeoJSONHierarchyLevel,
    rkiIds: Array<number>
  ): void {
    const nextLevel = GeoJSONHierarchy.get(currentLevel).next;
    const omitFeatureInteractions = nextLevel === undefined;
    const levelToFetchCaseDataFor =
      nextLevel === undefined ? currentLevel : nextLevel;

    // get all necessary data at once
    Promise.all([
      this.geoJsonService.getData(levelToFetchCaseDataFor, rkiIds).toPromise(),
      this.getCaseData(levelToFetchCaseDataFor, rkiIds).toPromise()
      // go display it
    ]).then(([featureCollection, caseData]) => {
      const layer = this.buildGeoJsonLayer({
        featureCollection,
        level: levelToFetchCaseDataFor,
        caseData,
        omitFeatureInteractions
      });
      this.layers = [layer];
    });
  }

  /**
   * Get cases data to display.
   * @param level
   * @param rkiIds
   */
  private getCaseData(
    level: GeoJSONHierarchyLevel,
    rkiIds: Array<number>
  ): Observable<ICaseResponse[]> {
    const maxDate = this.store.selectSnapshot(MapState.maxDate);
    const minDate = moment(maxDate).subtract(2, 'days');
    const expectedIfr = this.store.selectSnapshot(MapState.expectedIfr);
    return this.casesService.getData(rkiIds, level, minDate, maxDate, undefined, expectedIfr);
  }

  /**
   * Build appropriate GeoJsonLayer from feature collection and cases data.
   *
   * @param featureCollection GeoJSON collection of the features to display
   * @param level
   * @param caseData
   * @param omitFeatureInteractions
   */
  buildGeoJsonLayer: (options: {
    featureCollection: FeatureCollection;
    level: GeoJSONHierarchyLevel;
    caseData?: ICaseResponse[];
    omitFeatureInteractions?: boolean;
  }) => GeoJSON = ({
    featureCollection,
    level,
    caseData,
    omitFeatureInteractions = false
  }) => {
    const geoJSONLayer = geoJSON(null, {
      // callback function for each feature's style
      style: this.style,
      // callback to attach a bunch of event handlers to each feature
      onEachFeature: this.onEachFeature(omitFeatureInteractions)
    });

    if (caseData && caseData.length > 0 && featureCollection !== undefined) {
      featureCollection.features.forEach(feature =>
        this.showCaseNumbersForArea(caseData, level, feature, geoJSONLayer)
      );
    }

    if (featureCollection) geoJSONLayer.addData(featureCollection);
    return geoJSONLayer;
  };

  /**
   * Find the correct response dataset for the feature. Create the cases marker
   * and make sure the map area will have the correct color.
   *
   * @param caseData
   * @param level
   * @param feature
   * @param geoJSONLayer
   */
  private showCaseNumbersForArea(
    caseData: ICaseResponse[],
    level: GeoJSONHierarchyLevel,
    feature: Feature,
    geoJSONLayer: GeoJSON<{ [p: string]: any } | null>
  ) {
    const caseResponse = this.findMatchingResponse(caseData, level, feature);
    if (caseResponse === undefined) {
      return;
    }

    this.ensureMapColor(caseResponse, feature);
    const casesMarker = new CasesMarker(feature, caseResponse).casesMarker;
    if (casesMarker) casesMarker.addTo(geoJSONLayer);
  }

  /**
   * From an array of {@link ICaseResponse}s, find the most recent response that
   * matches the given feature.
   */
  private findMatchingResponse(
    caseData: ICaseResponse[],
    level: GeoJSONHierarchyLevel,
    feature: Feature
  ) {
    const filteredData = caseData.filter(
      response =>
        response[`${level.toLowerCase()}Id`] ===
        feature.properties[`rki${level}Id`]
    );
    // in case more than one day was returned, use the most recent one
    return filteredData.reverse()[0];
  }

  /**
   * Set an attribute on the feature that the style callback will then pick up on.
   * Not great, I know, but best I have so far.
   *
   * @param caseResponse
   * @param feature
   */
  private ensureMapColor(caseResponse: ICaseResponse, feature: Feature): void {
    const activeInfections = caseResponse.infectionsActive;
    const population = caseResponse.population;
    feature.properties.casesPer100000 =
      (activeInfections / population) * PER_POPULATION_MULTIPLIER;
  }
}
