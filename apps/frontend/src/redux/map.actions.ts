import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, StateToken } from '@ngxs/store';
import {
  DEFAULT_DAYS_TO_SHOW,
  GeoJSONHierarchy,
  GeoJSONHierarchyLevel,
  RkiIdGermany,
  ExpectedIfr
} from '@we-vs-virus/interfaces';
import * as moment from 'moment';
import { Moment } from 'moment';

// Events to dispatch that influence the map state
export class SelectFeature {
  static readonly type = '[Map] Select Feature';

  constructor(public feature: any) {}
}

export class BackButtonClicked {
  static readonly type = '[Map] Back Button Clicked';
}

export class TotalTimeframeToggled {
  static readonly type = '[Charts] Show Total Timeframe Toggled';

  constructor(public isChecked: boolean) {}
}

export class IfrSelectionChanged {
  static readonly type = '[ModelConfig] Change Expected Ifr';

  constructor(public value: ExpectedIfr) {}
}

/**
 * What the map state looks like.
 */
export interface MapStateModel {
  currentLevel: GeoJSONHierarchyLevel;
  rkiIds: Array<number>;
  minDate: Moment;
  maxDate: Moment;
  showTotalTimeframe: boolean;
  expectedIfr: number
}

const MAP_STATE_TOKEN = new StateToken<MapStateModel>('map');
const today = moment.utc().startOf('day');

@State<MapStateModel>({
  name: MAP_STATE_TOKEN,
  // our initial state
  defaults: {
    // TODO: dynamic default state
    currentLevel: GeoJSONHierarchyLevel.COUNTRY,
    rkiIds: [RkiIdGermany],
    maxDate: moment(today),
    minDate: moment(today).subtract(DEFAULT_DAYS_TO_SHOW, 'days'),
    showTotalTimeframe: true,
    expectedIfr: ExpectedIfr.LANCET
  }
})
/**
 * Defines selectors and actions on the map state.
 */
@Injectable()
export class MapState {
  @Selector()
  static rkiIds(state: MapStateModel) {
    return state.rkiIds;
  }

  @Selector()
  static currentLevel(state: MapStateModel) {
    return state.currentLevel;
  }

  @Selector()
  static maxDate(state: MapStateModel): Moment {
    return state.maxDate;
  }

  @Selector()
  static expectedIfr(state: MapStateModel): number {
    return state.expectedIfr;
  }

  /**
   * When a geoJSON feature was selected, the {@link currentLevel} gets set to that feature's hierarchy level.
   * The {@link rkiIds} are also updated based on that feature's IDs.
   * @param ctx NGXS state context from which we can access and change the state.
   * @param feature Leaflet feature/layer containing the selected GeoJSON feature.
   */
  @Action(SelectFeature)
  selectFeature(ctx: StateContext<MapStateModel>, { feature }: any) {
    const properties = feature.feature.properties;

    // Find the RKI IDs
    const rkiIds = [
      properties.rkiCountryId,
      properties.rkiStateId,
      properties.rkiCountyId
    ].filter(v => typeof v !== 'undefined');

    // Find out level, based on which RKI IDs are set
    let currentLevel = GeoJSONHierarchyLevel.COUNTRY;
    if (rkiIds.length <= 1) {
      currentLevel = GeoJSONHierarchyLevel.COUNTRY;
    } else if (rkiIds.length === 2) {
      currentLevel = GeoJSONHierarchyLevel.FEDERAL_STATES;
    } else if (rkiIds.length >= 3) {
      currentLevel = GeoJSONHierarchyLevel.COUNTIES;
    }

    // Update state
    const state = ctx.getState();
    ctx.setState({
      ...state,
      rkiIds,
      currentLevel
    });
  }

  /**
   * Move the map state one level up the {@link GeoJSONHierarchy}.
   * @param ctx NGXS state context.
   */
  @Action(BackButtonClicked)
  backButtonClicked(ctx: StateContext<MapStateModel>) {
    const state = ctx.getState();

    // Find correct level to return to
    const previousLevel = GeoJSONHierarchy.get(state.currentLevel).previous;

    // Remove IDs accordingly
    const rkiIds = [...state.rkiIds];
    rkiIds.pop();

    // Update state
    ctx.setState({
      ...state,
      rkiIds,
      currentLevel: previousLevel
    });
  }

  @Action(TotalTimeframeToggled)
  totalTimeframeToggled(ctx: StateContext<MapStateModel>, { isChecked }: TotalTimeframeToggled) {
    const state = ctx.getState();
    // Update state
    ctx.setState({
      ...state,
      showTotalTimeframe: isChecked
    });
  }

  @Action(IfrSelectionChanged)
  ifrRadioButtonsChanged(ctx: StateContext<MapStateModel>, { value }: IfrSelectionChanged) {
    const state = ctx.getState();
    // Update state
    ctx.setState({
      ...state,
      expectedIfr: value
    });
  }
}
