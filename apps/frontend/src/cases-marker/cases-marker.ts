import { divIcon, marker, Marker, PointExpression } from 'leaflet';
import { Feature, MultiPolygon, Polygon } from 'geojson';
import { ICaseResponse } from '@we-vs-virus/interfaces';
import polylabel from 'polylabel';
import * as turf from '@turf/turf';

/**
 * Build custom marker displaying reported & unreported cases
 */
export class CasesMarker {
  public casesMarker: Marker;

  constructor(feature: Feature, caseResponse: ICaseResponse) {
    this.casesMarker = this.buildCasesMarker(feature, caseResponse);
  }

  private buildCasesMarker: (
    feature: Feature,
    caseResponse: ICaseResponse
  ) => Marker = (feature, caseResponse) => {
    const cases = {
      reported: Math.round(caseResponse.casesActive),
      unreported: Math.round(caseResponse.infectionsActive)
    };
    const baseClass = 'cases-marker';
    const iconSize: PointExpression = [
      (Math.max(cases.reported, cases.unreported) + '').length * 12, // each digit gets 12px width
      32
    ];
    const iconBox = divIcon({
      iconSize,
      iconAnchor: [iconSize[0] / 2, iconSize[1] + 5],
      html: `
        <div class="${baseClass}__cases ${baseClass}__cases--reported">
          ${cases.reported}
        </div>
        <div class="${baseClass}__cases ${baseClass}__cases--unreported">
          ${cases.unreported}
        </div>
      `,
      className: baseClass
    });

    // calculate box position (feature's point of inaccessibility)
    const poa = this.getFeaturePOA(feature);

    // build marker and return it
    return poa
      ? marker(
          {
            lat: poa[1],
            lng: poa[0]
          },
          {
            icon: iconBox,
            interactive: false
          }
        )
      : null;
  };

  /**
   * Helper method to find the point of inaccessibility of a Geo JSON feature
   */
  private getFeaturePOA: (feature: Feature) => number[] = feature => {
    let poa = null;
    // first, find out largest polygon ("island") if we have a MultiPolygon
    const polygon =
      feature.geometry.type === 'MultiPolygon'
        ? this.multi2simple(feature.geometry)
        : feature.geometry;

    if (polygon.type === 'Polygon') {
      // compute pole of inaccessibility for largest polygon
      poa = polylabel(polygon.coordinates, 1.0);
    }
    return poa;
  };

  /**
   * Helper method that takes a MultiPolygon and return its largest Polygon by area
   */
  private multi2simple = function(multiPolygon: MultiPolygon): Polygon {
    const { maxAreaCoordinates } = multiPolygon.coordinates.reduce(
      ({ maxAreaCoordinates, maxArea }, coordinates) => {
        // convert to turf polygon
        const turfPolygon = turf.polygon(coordinates);
        // calc area
        const area = turf.area(turfPolygon);

        // larger area found
        if (area > maxArea) {
          maxArea = area;
          maxAreaCoordinates = coordinates;
        }

        // return max area info
        return {
          maxAreaCoordinates,
          maxArea
        };
      },
      {
        maxAreaCoordinates: null,
        maxArea: 0
      }
    );

    return {
      type: 'Polygon',
      coordinates: maxAreaCoordinates
    };
  };
}
