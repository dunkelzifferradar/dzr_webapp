# Build the Frontend
FROM node:10.20.1-stretch AS builder

WORKDIR /app
COPY package.json /app/
COPY package-lock.json /app/
COPY apps/ /app/apps/
COPY libs/ /app/libs/
COPY tools/ /app/tools/
COPY *.json /app/
COPY *.js /app/

# check node version
RUN node --version && \
  # update npm
  npm install --global npm@6 && \
  # prevent interactive prompt for angular cli analytics
  export NG_CLI_ANALYTICS=false && \
  # install required node packages
  npm ci && \
  # run the build script
  npm run build:backend && \
  # get rid of some stuff that we don't need in this image anyways
  rm -rf node_modules


# the container that will actually run the built app
FROM node:10.20.1-stretch-slim

WORKDIR /app

# copy artifact build from the 'build environment'
COPY --from=builder /app/dist/apps/backend /app
COPY package.json /app
COPY package-lock.json /app

RUN npm ci --production

EXPOSE 3333 3333

ENV NODE_ENV="production" \
  LOG_LEVEL="warn" \
  DB_URL="localhost" \
  DB_PORT="5432" \
  DB_USER="" \
  DB_PASS="" \
  DB_NAME=""

CMD ["node", "main.js"]
