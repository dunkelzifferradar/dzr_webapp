import { Module } from '@nestjs/common';
import { CasesStateController } from './cases-state.controller';
import { CasesStateService } from './cases-state.service';

@Module({
  imports: [],
  controllers: [CasesStateController],
  providers: [CasesStateService]
})
export class CasesStateModule {}
