import { Controller, Get, Param, Query, HttpException } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { CasesStateService } from './cases-state.service';
import { ICaseState } from '@we-vs-virus/interfaces';

@Controller('cases-state')
export class CasesStateController {
  constructor(private readonly casesStateService: CasesStateService) {}

  @Get('/:modelId/:countryId')
  @ApiOperation({ summary: 'Get cases on county level for a given country.' })
  public async getCasesCountry(
    @Param('modelId') modelId: number,
    @Param('countryId') countryId: number,
    @Query('minDate') minDate: number,
    @Query('maxDate') maxDate: number,
    @Query('aggregate') aggregate: boolean,
    @Query('expectedIfr') expectedIfr: number,
    @Query('paramInfectSymp') paramInfectSymp: number,
    @Query('paramInfectAsymp') paramInfectAsymp: number
  ) {
    return await this.casesStateService.getCasesStateByCountry(
      +modelId,
      +countryId,
      new Date(minDate),
      new Date(maxDate),
      aggregate,
      expectedIfr,
      paramInfectSymp,
      paramInfectAsymp
    );
  }

  @Get('/:modelId/:countryId/:stateId')
  @ApiOperation({
    summary: "Get cases on county level for a given country's state."
  })
  public async getCasesState(
    @Param('modelId') modelId: number,
    @Param('countryId') countryId: number,
    @Param('stateId') stateId: number,
    @Query('minDate') minDate: number,
    @Query('maxDate') maxDate: number,
    @Query('aggregate') aggregate: boolean,
    @Query('expectedIfr') expectedIfr: number,
    @Query('paramInfectSymp') paramInfectSymp: number,
    @Query('paramInfectAsymp') paramInfectAsymp: number
  ) {
    return await this.casesStateService.getCasesStateByCountryState(
      +modelId,
      +countryId,
      +stateId,
      new Date(minDate),
      new Date(maxDate),
      aggregate,
      expectedIfr,
      paramInfectSymp,
      paramInfectAsymp
    );
  }
}
