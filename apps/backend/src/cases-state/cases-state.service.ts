import { Injectable } from '@nestjs/common';
import { CaseStateEntity } from './entity/caseState.entity';
import { ICaseState } from '@we-vs-virus/interfaces';
import { Between, getRepository } from 'typeorm';

@Injectable()
export class CasesStateService {
  public async getCasesStateByCountry(
    modelId: number,
    countryId: number,
    minDate: Date,
    maxDate: Date,
    aggregate: boolean,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number
  ): Promise<ICaseState[]> {
    const caseCountryRepository = getRepository(CaseStateEntity);
    return await caseCountryRepository.find({
      where: {
        modelId: modelId,
        countryId: countryId,
        date: Between(minDate, maxDate),
        paramIfr: expectedIfr,
        paramInfectSymp: paramInfectSymp,
        paramInfectAsymp: paramInfectAsymp
      }
    });
  }

  public async getCasesStateByCountryState(
    modelId: number,
    countryId: number,
    stateId: number,
    minDate: Date,
    maxDate: Date,
    aggregate: boolean,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number
  ): Promise<ICaseState[]> {
    const caseStateRepository = getRepository(CaseStateEntity);
    return await caseStateRepository.find({
      where: {
        modelId: modelId,
        countryId: countryId,
        stateId: stateId,
        date: Between(minDate, maxDate),
        paramIfr: expectedIfr,
        paramInfectSymp: paramInfectSymp,
        paramInfectAsymp: paramInfectAsymp
      }
    });
  }
}
