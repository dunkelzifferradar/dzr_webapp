import { Column, Entity, Index } from 'typeorm';

@Index('sor_dim_county_pkey', ['id'], { unique: true })
@Entity('sor_dim_county', { schema: 'public' })
export class CountyEntity {
  @Column('integer', { primary: true, name: 'id' })
  id: number;

  @Column('smallint', { name: 'state_id', nullable: true })
  stateId: number | null;

  @Column('smallint', { name: 'type_id', nullable: true })
  typeId: number | null;

  @Column('character varying', { name: 'name', nullable: true, length: 48 })
  name: string | null;

  @Column('double precision', { name: 'area', nullable: true, precision: 53 })
  area: number | null;

  @Column('integer', { name: 'population', nullable: true })
  population: number | null;

  @Column('integer', { name: 'population_male', nullable: true })
  populationMale: number | null;

  @Column('integer', { name: 'population_female', nullable: true })
  populationFemale: number | null;

  @Column('integer', { name: 'population_density', nullable: true })
  populationDensity: number | null;
}
