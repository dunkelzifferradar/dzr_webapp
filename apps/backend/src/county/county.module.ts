import { Module } from '@nestjs/common';
import { CountyController } from './county.controller';
import { CountyService } from './county.service';

import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [],
  controllers: [CountyController],
  exports: [CountyService],
  providers: [CountyService]
})
export class CountyModule {}
