import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { CountyService } from './county.service';
import { ICounty } from '@we-vs-virus/interfaces';

@Controller('county')
export class CountyController {
  constructor(private readonly countyService: CountyService) {}

  @Get('/')
  @ApiOperation({ summary: 'Get county IDs.' })
  public async getCountyIDs(): Promise<ICounty[]> {
    return await this.countyService.getCounties();
  }
}
