import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';

import { CountyEntity } from './entity/county.entity';
import { ICounty } from '@we-vs-virus/interfaces';

@Injectable()
export class CountyService {
  public async getCounties(): Promise<ICounty[]> {
    const countyRepository = getRepository(CountyEntity);
    return await countyRepository.find();
  }
}
