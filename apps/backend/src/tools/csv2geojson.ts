import { IGeoJSON } from '@we-vs-virus/interfaces';

export function csv2geojson(rows): IGeoJSON {
  return {
    type: 'FeatureCollection',
    features: rows.map(r => {
      const { geometry, ...p } = r;
      return {
        type: 'Feature',
        geometry: geometry,
        properties: p
      };
    })
  };
}
