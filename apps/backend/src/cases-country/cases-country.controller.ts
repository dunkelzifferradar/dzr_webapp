import { Controller, Get, Param, Query, HttpException } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { CasesCountryService } from './cases-country.service';
import { ICaseCountry } from '@we-vs-virus/interfaces';

@Controller('cases-country')
export class CasesCountryController {
  constructor(private readonly casesCountryService: CasesCountryService) {}

  @Get('/:modelId/:countryId')
  @ApiOperation({ summary: 'Get cases on country level.' })
  public async getCasesCountry(
    @Param('modelId') modelId: number,
    @Param('countryId') countryId: number,
    @Query('minDate') minDate: number,
    @Query('maxDate') maxDate: number,
    @Query('aggregate') aggregate: boolean,
    @Query('expectedIfr') expectedIfr: number,
    @Query('paramInfectSymp') paramInfectSymp: number,
    @Query('paramInfectAsymp') paramInfectAsymp: number
  ) {
    return await this.casesCountryService.getCasesCountryByCountry(
      +modelId,
      +countryId,
      new Date(minDate),
      new Date(maxDate),
      aggregate,
      expectedIfr,
      paramInfectSymp,
      paramInfectAsymp
    );
  }
}
