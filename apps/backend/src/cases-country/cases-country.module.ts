import { Module } from '@nestjs/common';
import { CasesCountryController } from './cases-country.controller';
import { CasesCountryService } from './cases-country.service';

@Module({
  imports: [],
  controllers: [CasesCountryController],
  providers: [CasesCountryService]
})
export class CasesCountryModule {}
