import { Column, ViewEntity, PrimaryColumn } from 'typeorm';

@ViewEntity({
  name: 'dm_infections_country',
  expression: `
  select
    TARGET_ID as ID,
    NAME,
    POPULATION,
	  DATE,
    CASES_NEW,
    CASES_ACTIVE,
    CASES_SUM,
    INFECTIONS_NEW,
    INFECTIONS_ACTIVE,
    INFECTIONS_SUM,
    MODEL_ID,
    INFECT_SYMP as PARAM_INFECT_SYMP,
    INFECT_ASYMP as PARAM_INFECT_ASYMP,
    PARAM_IFR
  from (
    select TARGET_ID, DATE, mdl_result.MODEL_CONFIG_ID, CASES_NEW, CASES_ACTIVE, CASES_SUM,
          INFECTIONS_NEW, INFECTIONS_ACTIVE, INFECTIONS_SUM, country.population, country.name,
          model_id, infect_symp, infect_asymp, val as param_ifr
    from SOR_MDL_RESULT_COUNTRY mdl_result
    join SOR_DIM_COUNTRY country
    on mdl_result.target_id = country.id
      join (
          select cfg.ID, cfg.MODEL_ID, prm_time.val as INFECT_SYMP
          from SOR_DIM_MODEL_CONFIG cfg
          LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
          ON cfg.ID = prm_time.MODEL_CONFIG_ID
          LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
          ON prm_time.type_id = prm_time_type.id
          where prm_time_type.name = 'INFECTIOUS (SYMPTOMATIC)'
      ) cfg_symp
      on mdl_result.model_config_id = cfg_symp.id
      join (
          select cfg.ID, prm_time.val as INFECT_ASYMP
          from SOR_DIM_MODEL_CONFIG cfg
          LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
          ON cfg.ID = prm_time.MODEL_CONFIG_ID
          LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
          ON prm_time.type_id = prm_time_type.id
          where prm_time_type.name = 'INFECTIOUS (ASYMPTOMATIC)'
      ) cfg_asymp
      on mdl_result.model_config_id = cfg_asymp.id
      LEFT JOIN SOR_DIM_MODEL_PARAM_IFR prm_ifr
      ON cfg_symp.ID = prm_ifr.MODEL_CONFIG_ID
  ) data;
  `
})
export class CaseCountryEntity {
  @PrimaryColumn({ name: 'id' })
  public countryId!: number;

  @Column({ name: 'name' })
  public name!: string;

  @Column({ name: 'population' })
  public population!: number;

  @Column({ name: 'date' })
  public date!: string;

  @Column({ name: 'cases_new' })
  public casesNew!: number;

  @Column({ name: 'cases_active' })
  public casesActive!: number;

  @Column({ name: 'cases_sum' })
  public casesSum!: number;

  @Column({ name: 'infections_new' })
  public infectionsNew!: number;

  @Column({ name: 'infections_active' })
  public infectionsActive!: number;

  @Column({ name: 'infections_sum' })
  public infectionsSum!: number;

  @Column({ name: 'model_id' })
  public modelId!: number;

  @Column({ name: 'param_infect_symp' })
  public paramInfectSymp!: number;

  @Column({ name: 'param_infect_asymp' })
  public paramInfectAsymp!: number;

  @Column({ name: 'param_ifr' })
  public paramIfr!: number;
}
