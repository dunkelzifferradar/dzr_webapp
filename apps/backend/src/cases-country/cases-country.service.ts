import { Injectable } from '@nestjs/common';
import { CaseCountryEntity } from './entity/caseCountry.entity';
import { ICaseCountry } from '@we-vs-virus/interfaces';
import { Between, getRepository } from 'typeorm';

@Injectable()
export class CasesCountryService {
  public async getCasesCountryByCountry(
    modelId: number,
    countryId: number,
    minDate: Date,
    maxDate: Date,
    aggregate: boolean,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number
  ): Promise<ICaseCountry[]> {
    const caseCountryRepository = getRepository(CaseCountryEntity);
    return await caseCountryRepository.find({
      where: {
        modelId: modelId,
        countryId: countryId,
        date: Between(minDate, maxDate),
        paramIfr: expectedIfr,
        paramInfectSymp: paramInfectSymp,
        paramInfectAsymp: paramInfectAsymp
      }
    });
  }
}
