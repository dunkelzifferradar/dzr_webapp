import { Module } from '@nestjs/common';
import { DemodataController } from './demodata.controller';
import { DemodataService } from './demodata.service';

@Module({
  controllers: [DemodataController],
  providers: [DemodataService]
})
export class DemodataModule {}
