import { Test, TestingModule } from '@nestjs/testing';
import { DemodataController } from './demodata.controller';

describe('Demodata Controller', () => {
  let controller: DemodataController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DemodataController],
    }).compile();

    controller = module.get<DemodataController>(DemodataController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
