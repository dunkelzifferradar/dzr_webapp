import { Injectable } from '@nestjs/common';
import { IDemoDataResponse } from '@we-vs-virus/interfaces';
import * as moment from 'moment';

@Injectable()
export class DemodataService {
  getData(): IDemoDataResponse {
    const N: number = 10;
    const now = moment();

    let red = [];
    let green = [];
    let blue = [];
    for (let i = 0; i < N; i++) {
      red.push({
        value: Math.random() * 100,
        date: moment(now)
          .subtract(i, 'days')
          .format()
      });
      green.push({
        value: Math.random() * 100,
        date: moment(now)
          .subtract(i, 'days')
          .format()
      });
      blue.push({
        value: Math.random() * 100,
        date: moment(now)
          .subtract(i, 'days')
          .format()
      });
    }

    return {
      data: {
        red: red,
        green: green,
        blue: blue
      }
    };
  }
}
