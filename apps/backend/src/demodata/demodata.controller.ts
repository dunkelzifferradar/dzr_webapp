import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { DemodataService } from './demodata.service';

import { IDemoDataResponse } from '@we-vs-virus/interfaces';

@Controller('demodata')
export class DemodataController {
  constructor(private readonly demodataService: DemodataService) {}

  @Get('/')
  @ApiOperation({ summary: 'Get demo data' })
  getData(): IDemoDataResponse {
    return this.demodataService.getData();
  }
}
