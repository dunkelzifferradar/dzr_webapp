import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { DemodataModule } from '../demodata/demodata.module';
import { CasesCountryModule } from '../cases-country/cases-country.module';
import { CasesStateModule } from '../cases-state/cases-state.module';
import { CasesCountyModule } from '../cases-county/cases-county.module';

import { CountryModule } from '../country/country.module';
import { StateModule } from '../state/state.module';
import { CountyModule } from '../county/county.module';
import { ModelModule } from '../model/model.module';
import { GeoJSONCountryModule } from '../geojson-country/geojson-country.module';
import { GeoJSONCountyModule } from '../geojson-county/geojson-county.module';
import { GeoJSONStateModule } from '../geojson-state/geojson-state.module';

import { DatabaseModule } from '../database/database.module';
import { EnvironmentModule } from '../environment/environment.module';

@Module({
  imports: [
    EnvironmentModule,
    DatabaseModule,
    DemodataModule,
    CasesCountryModule,
    CasesStateModule,
    CasesCountyModule,
    CountryModule,
    StateModule,
    CountyModule,
    ModelModule,
    GeoJSONCountryModule,
    GeoJSONCountyModule,
    GeoJSONStateModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
