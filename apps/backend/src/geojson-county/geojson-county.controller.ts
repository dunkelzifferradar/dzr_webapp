import { Controller, Get, Param } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { GeoJSONCountyService } from './geojson-county.service';
import { IGeoJSON } from '@we-vs-virus/interfaces';

@Controller('geojson-county')
export class GeoJSONCountyController {
  constructor(private readonly geoJSONCountyService: GeoJSONCountyService) {}

  @Get('/:countryId')
  @ApiOperation({
    summary:
      'Get geojson data on county level for a country. It is not recommended to actually call that.'
  })
  public async getCountryGeoJSON(
    @Param('countryId') countryId: number
  ): Promise<IGeoJSON> {
    return await this.geoJSONCountyService.getByCountry(countryId);
  }

  @Get('/:countryId/:stateId')
  @ApiOperation({
    summary: "Get geojson data on county level for a country's state."
  })
  public async getStateGeoJSON(
    @Param('countryId') countryId: number,
    @Param('stateId') stateId: number
  ): Promise<IGeoJSON> {
    return await this.geoJSONCountyService.getByCountryState(
      countryId,
      stateId
    );
  }

  @Get('/:countryId/:stateId/:countyId')
  @ApiOperation({
    summary: "Get geojson data on county level for a country's state's county."
  })
  public async getCountyGeoJSON(
    @Param('countryId') countryId: number,
    @Param('stateId') stateId: number,
    @Param('countyId') countyId: number
  ): Promise<IGeoJSON> {
    return await this.geoJSONCountyService.getByCountryStateCounty(
      countryId,
      stateId,
      countyId
    );
  }
}
