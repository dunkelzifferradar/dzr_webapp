import { Column, Entity } from 'typeorm';

@Entity('sor_mp_geojson_county', { schema: 'public' })
export class GeoJSONCountyEntity {
  @Column('text', { name: 'geometry', nullable: true })
  geometry: string | null;

  @Column('smallint', { name: 'rki_country_id', nullable: true })
  rkiCountryId: number | null;

  @Column('smallint', { name: 'rki_state_id', nullable: true })
  rkiStateId: number | null;

  @Column('smallint', { name: 'rki_county_id', nullable: true })
  rkiCountyId: number | null;

  @Column('character varying', { name: 'nuts', nullable: true, length: 10, primary: true })
  nuts: string | null;

  @Column('character varying', {
    name: 'gen',
    nullable: true,
    length: 100
  })
  gen: string | null;

  @Column('character varying', {
    name: 'bundesland',
    nullable: true,
    length: 40
  })
  bundesland: string | null;

  @Column('bigint', { name: 'bundesland_code', nullable: true })
  bundeslandCode: string | null;

  @Column('bigint', { name: 'sdv_rs', nullable: true })
  sdvRs: string | null;

  @Column('character varying', {
    name: 'geo_point_2d',
    nullable: true,
    length: 30
  })
  geoPoint_2d: string | null;

  @Column('character varying', { name: 'bez', nullable: true, length: 25 })
  bez: string | null;

  @Column('integer', { name: 'ewz', nullable: true })
  ewz: number | null;

  @Column('numeric', { name: 'kfl', nullable: true })
  kfl: string | null;
}
