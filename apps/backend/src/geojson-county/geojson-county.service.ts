import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';

import { GeoJSONCountyEntity } from './entity/geojson-county.entity';
import { IGeoJSON } from '@we-vs-virus/interfaces';

import { csv2geojson } from '../tools/index';

@Injectable()
export class GeoJSONCountyService {
  public async getByCountry(countryId: number): Promise<IGeoJSON> {
    const repo = getRepository(GeoJSONCountyEntity);
    const res = await repo.find({
      where: { rkiCountryId: countryId }
    });
    return csv2geojson(res);
  }

  public async getByCountryState(
    countryId: number,
    stateId: number
  ): Promise<IGeoJSON> {
    const repo = getRepository(GeoJSONCountyEntity);
    const res = await repo.find({
      where: { rkiCountryId: countryId, rkiStateId: stateId }
    });
    return csv2geojson(res);
  }

  public async getByCountryStateCounty(
    countryId: number,
    stateId: number,
    countyId: number
  ): Promise<IGeoJSON> {
    const repo = getRepository(GeoJSONCountyEntity);
    const res = await repo.find({
      where: {
        rkiCountryId: countryId,
        rkiStateId: stateId,
        rkiCountyId: countyId
      }
    });
    return csv2geojson(res);
  }
}
