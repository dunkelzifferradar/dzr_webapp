import { Module } from '@nestjs/common';
import { GeoJSONCountyController } from './geojson-county.controller';
import { GeoJSONCountyService } from './geojson-county.service';

@Module({
  controllers: [GeoJSONCountyController],
  providers: [GeoJSONCountyService]
})
export class GeoJSONCountyModule {}
