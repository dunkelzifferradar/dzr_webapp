import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { DATABASE_ENTITIES } from './entities';
import { EnvironmentService } from '../environment/environment.service';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  constructor(private readonly environmentService: EnvironmentService) {}

  public async createTypeOrmOptions(
    connectionName?: string
  ): Promise<TypeOrmModuleOptions> {
    const db = this.environmentService.databaseConfiguration;

    return {
      ...db,
      type: 'postgres',
      schema: 'public',
      logging: ['log'],
      // NEVER set this to true in production
      synchronize: false,
      // @ts-ignore
      supportBigNumbers: true,
      bigNumberStrings: false,
      timezone: 'UTC',
      entities: DATABASE_ENTITIES
    };
  }
}
