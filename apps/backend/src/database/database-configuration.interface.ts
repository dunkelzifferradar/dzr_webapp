export interface IDatabaseConfiguration {
    readonly host: string;
    readonly port: number;
    readonly username: string;
    readonly password: string;
    readonly ssl: boolean;
    readonly database: string;
    readonly extra: { [key: string]: string | number };
}
