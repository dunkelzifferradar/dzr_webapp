import { CaseCountryEntity } from '../cases-country/entity/caseCountry.entity';
import { CaseStateEntity } from '../cases-state/entity/caseState.entity';
import { CaseCountyEntity } from '../cases-county/entity/caseCounty.entity';
import { CountryEntity } from '../country/entity/country.entity';
import { StateEntity } from '../state/entity/state.entity';
import { CountyEntity } from '../county/entity/county.entity';
import { ModelEntity } from '../model/entity/model.entity';
import { GeoJSONCountryEntity } from '../geojson-country/entity/geojson-country.entity';
import { GeoJSONCountyEntity } from '../geojson-county/entity/geojson-county.entity';
import { GeoJSONStateEntity } from '../geojson-state/entity/geojson-state.entity';

export const DATABASE_ENTITIES = [
  CaseCountryEntity,
  CaseStateEntity,
  CaseCountyEntity,
  CountryEntity,
  StateEntity,
  CountyEntity,
  ModelEntity,
  GeoJSONCountryEntity,
  GeoJSONCountyEntity,
  GeoJSONStateEntity
];
