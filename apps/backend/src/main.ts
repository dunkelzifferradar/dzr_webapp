/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app/app.module';
import { EnvironmentService } from './environment/environment.service';

async function bootstrap() {
  // server should always run in UTC to avoid issues
  process.env.TZ = 'UTC';

  const app = await NestFactory.create(AppModule);
  const env_config = await new EnvironmentService().environmentConfiguration;

  app.enableCors({
    origin: env_config.cors_urls
  });

  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  const port = process.env.port || 3333;

  const options = new DocumentBuilder()
    .setTitle('Unreported Cases')
    .setDescription('The Unreported Cases API description')
    .setVersion('0.1')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

bootstrap();
