import { Test, TestingModule } from '@nestjs/testing';
import { GeojsonStateService } from './geojson-state.service';

describe('GeojsonStateService', () => {
  let service: GeojsonStateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeojsonStateService],
    }).compile();

    service = module.get<GeojsonStateService>(GeojsonStateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
