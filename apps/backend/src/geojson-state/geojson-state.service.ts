import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';

import { GeoJSONStateEntity } from './entity/geojson-state.entity';
import { IGeoJSON } from '@we-vs-virus/interfaces';

import { csv2geojson } from '../tools/index';

@Injectable()
export class GeoJSONStateService {
  public async getByCountry(countryId: number): Promise<IGeoJSON> {
    const repo = getRepository(GeoJSONStateEntity);
    const res = await repo.find({
      where: { rkiCountryId: countryId }
    });
    return csv2geojson(res);
  }

  public async getByCountryState(
    countryId: number,
    stateId: number
  ): Promise<IGeoJSON> {
    const repo = getRepository(GeoJSONStateEntity);
    const res = await repo.find({
      where: { rkiCountryId: countryId, rkiStateId: stateId }
    });
    return csv2geojson(res);
  }
}
