import { Column, Entity } from 'typeorm';

@Entity('sor_mp_geojson_state', { schema: 'public' })
export class GeoJSONStateEntity {
  @Column('text', { name: 'geometry', nullable: true })
  geometry: string | null;

  @Column('smallint', { name: 'rki_state_id', nullable: true })
  rkiStateId: number | null;

  @Column('smallint', { name: 'rki_country_id', nullable: true })
  rkiCountryId: number | null;

  @Column('character varying', {
    primary: true,
    name: 'nuts',
    nullable: true,
    length: 10
  })
  nuts: string | null;

  @Column('character varying', { name: 'gen', nullable: true, length: 50 })
  gen: string | null;

  @Column('character varying', {
    name: 'geo_point_2d',
    nullable: true,
    length: 30
  })
  geoPoint_2d: string | null;

  @Column('bigint', { name: 'sdv_rs', nullable: true })
  sdvRs: string | null;

  @Column('character varying', { name: 'bez', nullable: true, length: 25 })
  bez: string | null;

  @Column('integer', { name: 'ewz', nullable: true })
  ewz: number | null;

  @Column('numeric', { name: 'kfl', nullable: true })
  kfl: string | null;
}
