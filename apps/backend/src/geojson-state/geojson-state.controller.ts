import { Controller, Get, Param } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { GeoJSONStateService } from './geojson-state.service';
import { IGeoJSON } from '@we-vs-virus/interfaces';

@Controller('geojson-state')
export class GeoJSONStateController {
  constructor(private readonly geoJSONStateService: GeoJSONStateService) {}

  @Get('/:countryId')
  @ApiOperation({
    summary: 'Get geojson data on state level for a given country.'
  })
  public async getStateGeoJSONByCountry(
    @Param('countryId') countryId: number
  ): Promise<IGeoJSON> {
    return await this.geoJSONStateService.getByCountry(countryId);
  }

  @Get('/:countryId/:stateId')
  @ApiOperation({
    summary: 'Get geojson data on state level for a given country and state'
  })
  public async getStateGeoJSONByState(
    @Param('countryId') countryId: number,
    @Param('stateId') stateId: number
  ): Promise<IGeoJSON> {
    return await this.geoJSONStateService.getByCountryState(countryId, stateId);
  }
}
