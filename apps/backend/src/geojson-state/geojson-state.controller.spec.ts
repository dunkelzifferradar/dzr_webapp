import { Test, TestingModule } from '@nestjs/testing';
import { GeojsonStateController } from './geojson-state.controller';

describe('GeojsonState Controller', () => {
  let controller: GeojsonStateController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GeojsonStateController],
    }).compile();

    controller = module.get<GeojsonStateController>(GeojsonStateController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
