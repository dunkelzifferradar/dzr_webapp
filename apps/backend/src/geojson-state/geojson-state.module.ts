import { Module } from '@nestjs/common';
import { GeoJSONStateController } from './geojson-state.controller';
import { GeoJSONStateService } from './geojson-state.service';

@Module({
  controllers: [GeoJSONStateController],
  providers: [GeoJSONStateService]
})
export class GeoJSONStateModule {}
