export interface EnvironmentConfiguration {
  readonly cors_urls: string[];
}
