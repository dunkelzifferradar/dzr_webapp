import { Injectable, Logger, Optional } from '@nestjs/common';
import { IDatabaseConfiguration } from '../database/database-configuration.interface';
import { EnvironmentConfiguration} from './environment-configuration.interface';

const POSTGRES_DEFAULT_PORT = 5432;
const CORS_DEFAULT_URLS = ['http://localhost:4200'];

@Injectable()
export class EnvironmentService {
  public databaseConfiguration!: IDatabaseConfiguration;
  public environmentConfiguration!: EnvironmentConfiguration;

  protected readonly logger = new Logger(EnvironmentService.name);

  constructor() {
    this.logger.setContext(EnvironmentService.name);
    this.logger.log('Init Environment Service');

    if (!this.isProduction) {
      this.logger.log('Load configuration from environment files.');

      const dotenv = require('dotenv');
      const res = dotenv.config('.env');
    }

    this.init();
  }

  public get port() {
    return +(process.env.PORT || 3333);
  }

  public get isProduction(): boolean {
    return process.env.NODE_ENV === 'production';
  }

  public get isDevelopment(): boolean {
    return process.env.NODE_ENV === 'development';
  }

  /**
   * Overwrite default values with environment settings
   */
  protected init() {
    const targetInfrastructure: string =
      process.env.TARGET_INFRASTRUCTURE || 'default';

    switch (targetInfrastructure.toLowerCase()) {
      default:
        this.readDefaultCORSConfiguration();
        this.readDefaultDBConfiguration();
        this.logger.log('Target infrastructure default');
        break;
    }
  }

  private readDefaultDBConfiguration() {
    const host = process.env.DB_URL;
    if (!host) {
      throw new Error('Please set "DB_URL"');
    }

    const port = +(process.env.DB_PORT || POSTGRES_DEFAULT_PORT);

    const username = process.env.DB_USER;
    if (!username) {
      throw new Error('Please set "DB_USER"');
    }

    const password = process.env.DB_PASS;
    if (!password) {
      throw new Error('Please set "DB_PASS"');
    }

    const database = process.env.DB_NAME;
    if (!database) {
      throw new Error('Please set "DB_NAME"');
    }

    const connectionLimit = +(process.env.DB_CONNECTION_LIMIT || 100);
    const sslEnabledString = process.env.DB_SSL || 'false';
    let sslEnabled = false;
    if (sslEnabledString === 'true') {
      sslEnabled = true;
    }

    this.databaseConfiguration = {
      database,
      host,
      port,
      password,
      username,
      ssl: sslEnabled,
      extra: {
        connectionLimit: connectionLimit
      }
    };
  }

  private readDefaultCORSConfiguration() {
    const cors_urls_string = process.env.CORS_URLS
    if (cors_urls_string) {
      const cors_urls = cors_urls_string.split(',').map(s => s.trim()).filter((value: string, index: number, array: string[]) => Boolean(value));
      if (cors_urls.length && cors_urls.length > 0) {
        this.environmentConfiguration = {
          cors_urls: cors_urls
        };
        return
      }
    }

    console.warn('"CORS_URLS" not set, using default URLs')
    this.environmentConfiguration = {
      cors_urls: CORS_DEFAULT_URLS
    };
  }

}
