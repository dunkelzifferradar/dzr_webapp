import { Module, Logger, Global } from '@nestjs/common';
import { EnvironmentService } from './environment.service';

@Global()
@Module({
  imports: [],
  providers: [Logger, EnvironmentService],
  exports: [EnvironmentService]
})
export class EnvironmentModule {}
