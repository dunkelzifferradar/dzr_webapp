import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { ModelService } from './model.service';
import { IModel } from '@we-vs-virus/interfaces';

@Controller('model')
export class ModelController {
  constructor(private readonly modelService: ModelService) {}

  @Get('/')
  @ApiOperation({ summary: 'Get model IDs.' })
  public async getModelIDs(): Promise<IModel[]> {
    return await this.modelService.getModels();
  }
}
