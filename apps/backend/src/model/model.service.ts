import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';

import { ModelEntity } from './entity/model.entity';
import { IModel } from '@we-vs-virus/interfaces';

@Injectable()
export class ModelService {
  public async getModels(): Promise<IModel[]> {
    const modelRepository = getRepository(ModelEntity);
    return await modelRepository.find();
  }
}
