import { Column, Entity, Index } from 'typeorm';

@Index('sor_dim_model_pkey', ['id'], { unique: true })
@Entity('sor_dim_model', { schema: 'public' })
export class ModelEntity {
  @Column('smallint', { primary: true, name: 'id' })
  id: number;

  @Column('character varying', { name: 'name', nullable: true, length: 100 })
  name: string | null;
}
