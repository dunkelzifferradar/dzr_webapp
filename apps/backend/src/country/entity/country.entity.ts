import { Column, Entity, Index } from 'typeorm';

@Index('sor_dim_country_pkey', ['id'], { unique: true })
@Index('sor_dim_country_name_key', ['name'], { unique: true })
@Entity('sor_dim_country', { schema: 'public' })
export class CountryEntity {
  @Column('smallint', { primary: true, name: 'id' })
  id: number;

  @Column('character varying', { name: 'name', nullable: true, length: 64 })
  name: string | null;

  @Column('integer', { name: 'population', nullable: true })
  population: number | null;
}
