import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';

import { CountryEntity } from './entity/country.entity';
import { ICountry } from '@we-vs-virus/interfaces';

@Injectable()
export class CountryService {
  public async getCountries(): Promise<ICountry[]> {
    const countryRepository = getRepository(CountryEntity);
    return await countryRepository.find();
  }
}
