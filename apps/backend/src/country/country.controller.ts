import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { CountryService } from './country.service';
import { ICountry } from '@we-vs-virus/interfaces';

@Controller('country')
export class CountryController {
  constructor(private readonly countryService: CountryService) {}

  @Get('/')
  @ApiOperation({ summary: 'Get country IDs.' })
  public async getCountryIDs(): Promise<ICountry[]> {
    return await this.countryService.getCountries();
  }
}
