import { Injectable } from '@nestjs/common';
//import { CaseCountryEntity } from './entity/caseCountry.entity';
//import { CaseStateEntity } from './entity/caseState.entity';
import { CaseCountyEntity } from './entity/caseCounty.entity';
import { ICaseCounty } from '@we-vs-virus/interfaces';
import { Between, getRepository } from 'typeorm';

@Injectable()
export class CasesCountyService {
  public async getCasesCountyByCountry(
    modelId: number,
    countryId: number,
    minDate: Date,
    maxDate: Date,
    aggregate: boolean,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number
  ): Promise<ICaseCounty[]> {
    const caseCountryRepository = getRepository(CaseCountyEntity);
    return await caseCountryRepository.find({
      where: {
        modelId: modelId,
        countryId: countryId,
        date: Between(minDate, maxDate),
        paramIfr: expectedIfr,
        paramInfectSymp: paramInfectSymp,
        paramInfectAsymp: paramInfectAsymp
      }
    });
  }

  public async getCasesCountyByCountryState(
    modelId: number,
    countryId: number,
    stateId: number,
    minDate: Date,
    maxDate: Date,
    aggregate: boolean,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number
  ): Promise<ICaseCounty[]> {
    const caseStateRepository = getRepository(CaseCountyEntity);
    return await caseStateRepository.find({
      where: {
        modelId: modelId,
        countryId: countryId,
        stateId: stateId,
        date: Between(minDate, maxDate),
        paramIfr: expectedIfr,
        paramInfectSymp: paramInfectSymp,
        paramInfectAsymp: paramInfectAsymp
      }
    });
  }

  public async getCasesCountyByCountryStateCounty(
    modelId: number,
    countryId: number,
    stateId: number,
    countyId: number,
    minDate: Date,
    maxDate: Date,
    aggregate: boolean,
    expectedIfr: number,
    paramInfectSymp: number,
    paramInfectAsymp: number
  ): Promise<ICaseCounty[]> {
    const caseCountyRepository = getRepository(CaseCountyEntity);
    return await caseCountyRepository.find({
      where: [
        {
          modelId: modelId,
          countryId: countryId,
          stateId: stateId,
          countyId: countyId,
          date: Between(minDate, maxDate),
          paramIfr: expectedIfr,
          paramInfectSymp: paramInfectSymp,
          paramInfectAsymp: paramInfectAsymp
        }
      ]
    });
  }
}
