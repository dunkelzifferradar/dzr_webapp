import { Controller, Get, Param, Query, HttpException } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { CasesCountyService } from './cases-county.service';
import { ICaseCounty } from '@we-vs-virus/interfaces';

@Controller('cases-county')
export class CasesCountyController {
  constructor(private readonly casesCountyService: CasesCountyService) {}

  @Get('/:modelId/:countryId')
  @ApiOperation({ summary: 'Get cases on county level for a given country.' })
  public async getCasesCountry(
    @Param('modelId') modelId: number,
    @Param('countryId') countryId: number,
    @Query('minDate') minDate: number,
    @Query('maxDate') maxDate: number,
    @Query('aggregate') aggregate: boolean,
    @Query('expectedIfr') expectedIfr: number,
    @Query('paramInfectSymp') paramInfectSymp: number,
    @Query('paramInfectAsymp') paramInfectAsymp: number
  ) {
    return await this.casesCountyService.getCasesCountyByCountry(
      +modelId,
      +countryId,
      new Date(minDate),
      new Date(maxDate),
      aggregate,
      expectedIfr,
      paramInfectSymp,
      paramInfectAsymp
    );
  }

  @Get('/:modelId/:countryId/:stateId')
  @ApiOperation({
    summary: "Get cases on county level for a given country's state."
  })
  public async getCasesState(
    @Param('modelId') modelId: number,
    @Param('countryId') countryId: number,
    @Param('stateId') stateId: number,
    @Query('minDate') minDate: number,
    @Query('maxDate') maxDate: number,
    @Query('aggregate') aggregate: boolean,
    @Query('expectedIfr') expectedIfr: number,
    @Query('paramInfectSymp') paramInfectSymp: number,
    @Query('paramInfectAsymp') paramInfectAsymp: number
  ) {
    return await this.casesCountyService.getCasesCountyByCountryState(
      +modelId,
      +countryId,
      +stateId,
      new Date(minDate),
      new Date(maxDate),
      aggregate,
      expectedIfr,
      paramInfectSymp,
      paramInfectAsymp
    );
  }

  @Get('/:modelId/:countryId/:stateId/:countyId/')
  @ApiOperation({
    summary: "Get cases on county level for a given country's state's county."
  })
  public async getCasesCounty(
    @Param('modelId') modelId: number,
    @Param('countryId') countryId: number,
    @Param('stateId') stateId: number,
    @Param('countyId') countyId: number,
    @Query('minDate') minDate: string,
    @Query('maxDate') maxDate: string,
    @Query('aggregate') aggregate: boolean,
    @Query('expectedIfr') expectedIfr: number,
    @Query('paramInfectSymp') paramInfectSymp: number,
    @Query('paramInfectAsymp') paramInfectAsymp: number
  ): Promise<ICaseCounty[]> {
    return await this.casesCountyService.getCasesCountyByCountryStateCounty(
      +modelId,
      +countryId,
      +stateId,
      +countyId,
      new Date(minDate),
      new Date(maxDate),
      aggregate,
      expectedIfr,
      paramInfectSymp,
      paramInfectAsymp
    );
  }
}
