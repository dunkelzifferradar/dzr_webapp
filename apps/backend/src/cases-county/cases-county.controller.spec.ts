import { Test, TestingModule } from '@nestjs/testing';
import { CasesController } from './cases-county.controller';

describe('CaseCounty Controller', () => {
  let controller: CasesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CasesController]
    }).compile();

    controller = module.get<CasesController>(CasesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
