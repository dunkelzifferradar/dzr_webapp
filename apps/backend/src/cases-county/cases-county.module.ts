import { Module } from '@nestjs/common';
import { CasesCountyController } from './cases-county.controller';
import { CasesCountyService } from './cases-county.service';

@Module({
  imports: [],
  controllers: [CasesCountyController],
  providers: [CasesCountyService]
})
export class CasesCountyModule {}
