import { Controller, Get, Param } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { GeoJSONCountryService } from './geojson-country.service';
import { IGeoJSON } from '@we-vs-virus/interfaces';

@Controller('geojson-country')
export class GeoJSONCountryController {
  constructor(private readonly geoJSONCountryService: GeoJSONCountryService) {}

  @Get('/')
  @ApiOperation({
    summary: 'Get geojson data on country level for all countries.'
  })
  public async getCountryGeoJSON(): Promise<IGeoJSON> {
    return await this.geoJSONCountryService.getAll();
  }

  @Get('/:countryId')
  @ApiOperation({
    summary: 'Get geojson data on country level for a given country.'
  })
  public async getCountryGeoJSONByCountry(
    @Param('countryId') countryId: number
  ): Promise<IGeoJSON> {
    return await this.geoJSONCountryService.getByCountry(countryId);
  }
}
