import { Column, Entity } from 'typeorm';

@Entity('sor_mp_geojson_country', { schema: 'public' })
export class GeoJSONCountryEntity {
  @Column('character varying', {
    primary: true,
    name: 'name',
    nullable: true,
    length: 20
  })
  name: string | null;

  @Column('smallint', { name: 'rki_country_id', nullable: true })
  rkiCountryId: number | null;

  @Column('text', { name: 'geometry', nullable: true })
  geometry: string | null;
}
