import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import { csv2geojson } from '../tools/index';

import { GeoJSONCountryEntity } from './entity/geojson-country.entity';
import { IGeoJSON } from '@we-vs-virus/interfaces';

@Injectable()
export class GeoJSONCountryService {
  public async getAll(): Promise<IGeoJSON> {
    const repo = getRepository(GeoJSONCountryEntity);
    const res = await repo.find();
    return csv2geojson(res);
  }

  public async getByCountry(countryId: number): Promise<IGeoJSON> {
    const repo = getRepository(GeoJSONCountryEntity);
    const res = await repo.find({
      where: { rkiCountryId: countryId }
    });
    return csv2geojson(res);
  }
}
