import { Module } from '@nestjs/common';
import { GeoJSONCountryController } from './geojson-country.controller';
import { GeoJSONCountryService } from './geojson-country.service';

@Module({
  controllers: [GeoJSONCountryController],
  providers: [GeoJSONCountryService]
})
export class GeoJSONCountryModule {}
