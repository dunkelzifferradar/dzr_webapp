import { Test, TestingModule } from '@nestjs/testing';
import { GeoJSONCountryController } from './geojson-country.controller';

describe('GeoJSONCountry Controller', () => {
  let controller: GeoJSONCountryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GeoJSONCountryController]
    }).compile();

    controller = module.get<GeoJSONCountryController>(GeoJSONCountryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
