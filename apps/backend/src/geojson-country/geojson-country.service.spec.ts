import { Test, TestingModule } from '@nestjs/testing';
import { GeoJSONCountryService } from './geojson-country.service';

describe('GeoJSONCountryService', () => {
  let service: GeoJSONCountryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeoJSONCountryService]
    }).compile();

    service = module.get<GeoJSONCountryService>(GeoJSONCountryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
