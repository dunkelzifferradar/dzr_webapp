import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';

import { StateEntity } from './entity/state.entity';
import { IState } from '@we-vs-virus/interfaces';

@Injectable()
export class StateService {
  public async getStates(): Promise<IState[]> {
    const stateRepository = getRepository(StateEntity);
    return await stateRepository.find();
  }

  public async getStateById(stateId: number): Promise<IState> {
    const stateRepository = getRepository(StateEntity);
    return await stateRepository.findOne({ where: { id: stateId } });
  }
}
