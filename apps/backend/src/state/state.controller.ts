import { Controller, Get, Param } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { StateService } from './state.service';
import { IState } from '@we-vs-virus/interfaces';

@Controller('state')
export class StateController {
  constructor(private readonly stateService: StateService) {}

  @Get('/')
  @ApiOperation({ summary: 'Get state IDs.' })
  public async getStateIDs(): Promise<IState[]> {
    return await this.stateService.getStates();
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Get state info by state ID.' })
  public async getState(@Param('id') stateId: number) {
    return await this.stateService.getStateById(stateId);
  }
}
