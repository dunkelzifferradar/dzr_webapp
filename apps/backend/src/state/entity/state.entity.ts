import { Column, Entity, Index } from 'typeorm';

@Index('sor_dim_state_pkey', ['id'], { unique: true })
@Entity('sor_dim_state', { schema: 'public' })
export class StateEntity {
  @Column('smallint', { primary: true, name: 'id' })
  id: number;

  @Column('smallint', { name: 'country_id', nullable: true })
  countryId: number | null;

  @Column('character varying', { name: 'name', nullable: true, length: 24 })
  name: string | null;

  @Column('character', { name: 'abbrev', nullable: true, length: 2 })
  abbrev: string | null;

  @Column('integer', { name: 'area', nullable: true })
  area: number | null;

  @Column('integer', { name: 'population', nullable: true })
  population: number | null;

  @Column('integer', { name: 'population_density', nullable: true })
  populationDensity: number | null;

  @Column('double precision', {
    name: 'foreign_pctg',
    nullable: true,
    precision: 53
  })
  foreignPctg: number | null;
}
