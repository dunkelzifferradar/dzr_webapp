import { GeoJSONHierarchyLevel } from './enums';

/**
 * Keeps various info about levels we've needed so far in one place. Not the sleekest, I know.
 */
export const GeoJSONHierarchy: Map<
  GeoJSONHierarchyLevel,
  {
    name: string;
    value: GeoJSONHierarchyLevel;
    levelIndex: number;
    next: GeoJSONHierarchyLevel;
    previous: GeoJSONHierarchyLevel;
  }
> = new Map([
  [
    GeoJSONHierarchyLevel.COUNTRY,
    {
      name: 'COUNTRY',
      value: GeoJSONHierarchyLevel.COUNTRY,
      levelIndex: 0,
      next: GeoJSONHierarchyLevel.FEDERAL_STATES,
      previous: undefined
    }
  ],
  [
    GeoJSONHierarchyLevel.FEDERAL_STATES,
    {
      name: 'FEDERAL_STATES',
      value: GeoJSONHierarchyLevel.FEDERAL_STATES,
      levelIndex: 1,
      next: GeoJSONHierarchyLevel.COUNTIES,
      previous: GeoJSONHierarchyLevel.COUNTRY
    }
  ],
  [
    GeoJSONHierarchyLevel.COUNTIES,
    {
      name: 'COUNTIES',
      value: GeoJSONHierarchyLevel.COUNTIES,
      levelIndex: 2,
      next: undefined,
      previous: GeoJSONHierarchyLevel.FEDERAL_STATES
    }
  ]
]);

export const RkiIdGermany = 0;

export const DEFAULT_DAYS_TO_SHOW: number = 42;

export const CHART_COLOR_INFECTIONS: string = '#37474f'; // corresponds to mat-palette($mat-blue-grey, 800);

export const CHART_COLOR_CASES: string = '#fff';

export const CHART_COLOR_UNREPORTED: string = '#759caf';

export const PER_POPULATION_MULTIPLIER: number = 100000;
