export enum GeoJSONResolution {
  VERY_HIGH = '1_sehr_hoch',
  HIGH = '2_hoch',
  MEDIUM = '3_mittel',
  LOW = '4_niedrig'
}

export enum GeoJSONHierarchyLevel {
  COUNTRY = 'Country',
  FEDERAL_STATES = 'State',
  COUNTIES = 'County'
}

export enum ExpectedIfr {
  LANCET = 0.00657,
  HEINSBERG = 0.00358,
  IOANNIDIS = 0.0024
};
