export interface ICountry {
  id: number;
  name: string | null;
  population: number | null;
}
