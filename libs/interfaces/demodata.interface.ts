export interface INumDate {
  value: number;
  date: Date;
}

export interface IDemoDataResponse {
  data: {
    red: Array<INumDate>;
    green: Array<INumDate>;
    blue: Array<INumDate>;
  };
}
