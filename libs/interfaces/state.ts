export interface IState {
  id: number;
  countryId: number | null;
  name: string | null;
  abbrev: string | null;
  area: number | null;
  population: number | null;
  populationDensity: number | null;
  foreignPctg: number | null;
}
