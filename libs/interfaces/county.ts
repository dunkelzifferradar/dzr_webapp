export interface ICounty {
  id: number;
  stateId: number | null;
  typeId: number | null;
  name: string | null;
  area: number | null;
  population: number | null;
  populationMale: number | null;
  populationFemale: number | null;
  populationDensity: number | null;
}
