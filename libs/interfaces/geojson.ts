export interface IGeoJSON {
  type: string;
  features: IGeoJSONCountry[] | IGeoJSONState[] | IGeoJSONCounty[];
}

export interface IGeoJSONCounty {
  type: string;
  properties: {
    rkiCountryId: number | null;
    rkiStateId: number | null;
    rkiCountyId: number | null;
    nuts: string | null;
    gen: string | null;
    bundesland: string | null;
    bundeslandCode: string | null;
    sdvRs: string | null;
    geoPoint_2d: string | null;
    bez: string | null;
    ewz: number | null;
    kfl: string | null;
  };
  geometry: object;
}

export interface IGeoJSONState {
  type: string;
  properties: {
    rkiStateId: number | null;
    nuts: string | null;
    gen: string | null;
    geoPoint_2d: string | null;
    sdvRs: string | null;
    bez: string | null;
    ewz: number | null;
    kfl: string | null;
  };
  geometry: string | null;
}

export interface IGeoJSONCountry {
  type: string;
  properties: {
    name: string | null;
    rkiCountryId: number | null;
  };
  geometry: string | null;
}
