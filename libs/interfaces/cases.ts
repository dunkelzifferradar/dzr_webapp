export interface ICaseResponse {
  /**
   * Model used to calculate cases and infections.
   */
  modelId: number;
  /**
   * ID of the country, currently only 0 for Germany, as found in RKI data.
   */
  countryId: number;
  /**
   * Name of the country/state/county.
   */
  name: string;
  /**
   * Population of the country/state/county.
   */
  population: number;
  /**
   * Date for which these numbers were calculated.
   */
  date: string;
  /**
   * New reported cases for that day. Corresponds to date reported to RKI but smoothed over by model.
   * Model fitting here is done to regularise in areas with low infection counts, and to smooth out the weekend effect.
   */
  casesNew: number;
  /**
   * Reported cases that the model considers to be (still) active on that day.
   * Active = assumed infectious (model 1).
   */
  casesActive: number;
  /**
   * Accumulated reported cases up to that day.
   */
  casesSum: number;
  /**
   * New infections (sum of reported and unreported) calculated for that day. Corresponds to date that the person enters
   * the "infectious" state in model 1.
   */
  infectionsNew: number;
  /**
   * Infections (sum of reported and unreported) that the model considers to be (already/still) active on that day.
   * Active = assumed infectious (model 1).
   */
  infectionsActive: number;
  /**
   * Accumulated infections (sum of reported and unreported) up to that day.
   */
  infectionsSum: number;
  /**
   * Model parameter (model 1): Estimated time in days that a person is infectious after
   *  - developing symptoms (for unreported cases).
   *  - being reported to RKI (for reported cases).
   */
  paramInfectSymp: number;
  /**
   * Model parameter (model 1): Estimated time in days that a person is infectious before developing symptoms.
   */
  paramInfectAsymp: number;
  /**
   * Model parameter (model 1): Estimated infection fatality ratio.
   */
  paramIfr: number;
}

export interface ICaseCountry extends ICaseResponse {
}

export interface ICaseState extends ICaseCountry {
  /**
   * ID of the state as found in RKI data.
   */
  stateId: number;
}

export interface ICaseCounty extends ICaseState {
  /**
   * ID of the county as found in RKI data.
   */
  countyId: number;
}
